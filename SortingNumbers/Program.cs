﻿using System;

namespace SortingNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = double.Parse(Console.ReadLine());
            double num2 = double.Parse(Console.ReadLine());
            double num3 = double.Parse(Console.ReadLine());
            double index = double.Parse(Console.ReadLine());

            if (num2 < num1)
            {
                index = num1;
                num1 = num2;
                num2 = index;
            }

            if (num3 < num1)
            {
                index = num1;
                num1 = num3;
                num3 = index;

            }

            if (num3 < num2)
            {
                index = num2;
                num2 = num3;
                num3 = index;

            }

            Console.WriteLine(num1);
            Console.WriteLine(num2);
            Console.WriteLine(num3);

        }
    }
}
