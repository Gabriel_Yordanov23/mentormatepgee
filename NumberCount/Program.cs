﻿using System;

namespace NumberCount
{
    class ProgramA
    {

        static void Main(string[] args)
        {
            
            double num1 = double.Parse(Console.ReadLine());
            double num2 = double.Parse(Console.ReadLine());
            double num3 = double.Parse(Console.ReadLine());
            double x = double.Parse(Console.ReadLine());

            if (num2 < num1)
            {
                x = num1;
                num1 = num2;
                num2 = x;

            }

            else if (num3 < num1)
            {
                x = num1;
                num1 = num3;
                num3 = x;

            }

            else if (num3 < num2)
            {
                x = num2;
                num2 = num3;
                num3 = x;

            }
            
        }
    }
}
