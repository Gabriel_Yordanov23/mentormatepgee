﻿using System;

namespace ConsoleApp1
{
    class Program
        
    {
        const int ALLOWED_AGE_TO_DRINK_ON_PUBLIC = 18;
        static void Main(string[] args)
        {
            int myCurentAge = int.Parse(Console.ReadLine());

            if (myCurentAge > 63)
            {
                Console.WriteLine("You are retired and you can't vote");
            }
            else if (myCurentAge < ALLOWED_AGE_TO_DRINK_ON_PUBLIC)
            {
                Console.WriteLine("You are too young to vote!");
            }
            else
            {
                Console.WriteLine("Your age is {0} and this is enough for you to vote!", myCurentAge);
            }
            
        }
    }
}
